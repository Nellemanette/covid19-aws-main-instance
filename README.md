# COVID19 Microsimulator - AWS Main Instance
---
## Description

Python interface to be used with the microsimulator [COVID19 GAMA Model Project](https://gitlab.com/covid19-projects/covid19-microsimulation/covid19-gama-model). It is ready to be deployed out of the box in AWS instances, but it can be used in a local environment too.

## Table of Contents

1. [Included in AWS installation](/docs/includes/README.md)
2. [Prerequisites](/docs/prerequisites/README.md)
3. [Installation](/docs/installation/README.md)
4. [Usage](/docs/usage/README.md)