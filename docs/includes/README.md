# COVID19 Microsimulator - AWS Main Instance

## Includes

- [Unzip](https://linux.die.net/man/1/unzip)
- [Python3.8](https://www.python.org/downloads/release/python-380/) :snake:
- [Pip](https://pypi.org/project/pip/)
- [VirtualEnv](https://pypi.org/project/virtualenv/) (named as `virtualenv-covid19`)
- [Jupyter Notebook](https://jupyter.org/) (with a cool dark theme, [monokai](https://medium.com/@rbmsingh/making-jupyter-dark-mode-great-5adaedd814db) :wink: )
    - [Matplotlib](https://matplotlib.org/)
    - [Numpy](https://numpy.org/)
    - [Seaborn](https://seaborn.pydata.org/)
    - [Graphviz](https://graphviz.org/)
    - [Pandas](https://pandas.pydata.org/)
    - [Geopandas](https://geopandas.org/)
    - [Subprocess](https://docs.python.org/3/library/subprocess.html)
    - [Pydeck](https://pypi.org/project/pydeck/)
    - [Keplergl](https://kepler.gl/)
    - [IPywidgets](https://ipywidgets.readthedocs.io/en/stable/)
    - [Tqdm](https://github.com/tqdm/tqdm)
    - [Plotly](https://plotly.com/)
    - [Boto3](https://github.com/boto/boto3)
- [GAMA Platform](https://gama-platform.github.io/) (<--- Its the simulator's framework!)
- [COVID19 GAMA Model Project](https://gitlab.com/covid19-projects/covid19-microsimulation/covid19-gama-model) (<--- Its the simulator's model!)

> ## [Back](/README.md)