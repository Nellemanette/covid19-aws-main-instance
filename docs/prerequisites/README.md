# COVID19 Microsimulator - AWS Main Instance

## Prerequisites

#### 1. EC2 user with roles:
- Specially `EC2_FullAccessRole` and `AWSServiceRoleForAmazonElasticFileSystem`.

![Roles](/docs/prerequisites/img/roles.png "Roles")

#### 2. A Security Group (SG) with its inbound rules:
- And set 2049 port for outbound too!

![Security Group Rules](/docs/prerequisites/img/security_group.png "Security Group Rules")
  
#### 3. An Elastic File System (EFS) volume, for shared persistance between main and secondary EC2 instances:
- It must have an Access Point

![EFS Access Point](/docs/prerequisites/img/efs_access_point.png "EFS Access Point")

- And all its "Mount Targets" must have the same SG added

![EFS Security Group](/docs/prerequisites/img/efs_security_group.png "EFS Security Group")

> ## [Back](/README.md)