# COVID19 Microsimulator - AWS Main Instance

## Usage

1. Connect to AWS instance:
    - `ssh -i "my-keypair.pem" ubuntu@the-public-instance-dns`
2. Activate virtualenv (if deactivated):
    - `source covid19-aws-main-instance/virtualenv-covid19/bin/activate`
3. Set the password (at first time):
    - `jupyter notebook password`
4. Set permissions:
    - `chmod 777 covid19-aws-main-instance/start_up_jupyter_notebook.sh`
5. Launch Jupyter Notebook server in background (default port: `8888`):
    - `./covid19-aws-main-instance/start_up_jupyter_notebook.sh`
6. Close the connection if necessary:
    - `exit`
7. Port forwarding in a local terminal (i.e. from remote `8888` to local `8090`):
    - `ssh -N -f -i "my-keypair.pem" -L localhost:8090:localhost:8888 ubuntu@the-public-instance-dns`
8. Connect to the Jupyter Notebook server in your favorite browser (`8090` in our example):
    - `http://localhost:8090/`
9.  Type the password if required
    - :no_mouth: :id:
10. Open a Jupyter Notebook and lets start
    - i.e. `AWS_Launcher.ipynb`.

## Extras

- `deactivate`
: to deactivate the virtualenv

- And don't forget to shutdown the AWS instance after usage!

> ## [Back](/README.md)