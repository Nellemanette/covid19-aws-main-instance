#!/bin/bash

sudo apt update &&
sudo apt upgrade -y &&
sudo apt install -y unzip &&

sudo apt install software-properties-common &&
sudo add-apt-repository ppa:deadsnakes/ppa -y &&
sudo apt install -y python3.8 &&
sudo apt install -y python3-pip &&

python3.8 -m pip install virtualenv &&
python3.8 -m virtualenv virtualenv-covid19 &&
source virtualenv-covid19/bin/activate &&

pip3 install --upgrade pip &&
pip3 install -r requirements.txt &&
pip3 install --upgrade notebook &&
pip3 install --upgrade jupyterthemes &&

jupyter contrib nbextension install --user &&
jupyter nbextension enable --py widgetsnbextension &&
jt -t monokai -f fira -fs 12 -nf ubuntu -nfs 12 -tf ubuntu -tfs 12 -N -kl -cursw 5 -cursc r -cellw 90%

START_JUPYTER_NOTEBOOK="./start_up_jupyter_notebook.sh"
. "$START_JUPYTER_NOTEBOOK"
