import os
import boto3
from botocore.config import Config
from src.core.execution import Execution
from src.core.simulator import AbstractSimulator
from src.headless.specification import HeadlessSpecification

class AWSAbstractSimulator(AbstractSimulator):
    def __init__(self, *args, **kwargs):
        super(AWSAbstractSimulator, self).__init__(*args, **kwargs)

    # SIMULATE
    def simulate(self, experiment):
        self.timestamp = self.get_timestamp()
        self.output_path = os.path.join(self.environment('efs_mount_path'), self.scenario_name(), 'results', self.timestamp)
        self.raw_userdata = open(os.path.join('src','cloud','instance_userdata.txt'), 'r').read()
        self.ec2_client = boto3.client('ec2', config=Config(
            region_name = self.environment('region_name'),
            signature_version = 'v4',
            retries = {'max_attempts': 10, 'mode': 'standard'}))

    # PRIVATE
    def __run_instance(self, instance_name, full_output_path, experiment):
        headless_file_path = os.path.join(full_output_path, 'headless.xml')
        
        self.model_parameters().\
            set_simulation_mode(type(self).mode_name()).\
            set_extra_parameters_file(self.environment().resources_path_for(self.scenario_name(), 'includes', 'extra_parameters.json')).\
            set_xml_headless_file_path(headless_file_path).\
            set_output_path(full_output_path).\
            set_resources_path(self.environment().resources_path_for(self.scenario_name()))
        HeadlessSpecification(headless_file_path, self).generate_for(experiment)
        execution = Execution.defined_by(headless_file_path)

        instance_conf = {}
        instance_conf['user_id'] = self.environment('user_id')
        instance_conf['group_id'] = self.environment('group_id')
        instance_conf['efs_volume_id'] = self.environment('efs_volume_id')
        instance_conf['efs_mount_path'] = self.environment('efs_mount_path')
        instance_conf['project_path'] = self.environment('project_path')
        instance_conf['java_jdk_path'] = self.environment('java_jdk_path')
        instance_conf['gama_headless_sh'] = self.environment('gama_headless_sh')
        instance_conf['headless_file_path'] = headless_file_path
        instance_conf['full_output_path'] = full_output_path
        userdata = self.raw_userdata.format(**instance_conf)
        
        response = self.ec2_client.run_instances(
            BlockDeviceMappings = [{'DeviceName': '/dev/sda1', 'Ebs': {'DeleteOnTermination': True, 'VolumeSize': 20, 'VolumeType': 'gp2'}}],
            ImageId = self.environment('secondary_ami_image_id'),
            InstanceType = self.environment('secondary_instance_type'),
            KeyName = self.environment('key_name'),
            MaxCount = 1,
            MinCount = 1,
            InstanceInitiatedShutdownBehavior = 'terminate',
            Monitoring = {'Enabled': False},
            SecurityGroups = [self.environment('security_group_id')],
            UserData = userdata)

        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            running_instances = response['Instances']
            for instance in running_instances:
                self.ec2_client.create_tags(\
                    Resources=[instance['InstanceId']], 
                    Tags=[{"Key":"Name", "Value":instance_name}])
        else:
            print(response['ResponseMetadata'])

        return execution