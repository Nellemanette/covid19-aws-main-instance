import os
from itertools import product
from src.headless.model_parameter import ModelParameter
from src.cloud.aws_abstract_simulator import AWSAbstractSimulator

class AWSParallelSimulator(AWSAbstractSimulator):
    def __init__(self, *args, **kwargs):
        super(AWSParallelSimulator, self).__init__(*args, **kwargs)

    @classmethod
    def mode_name(cls):
        return 'Parallel'

    # SIMULATE
    def simulate(self, experiment):
        super(AWSParallelSimulator, self).simulate(experiment)
        parameters_to_explore = experiment.at('parameters_to_explore')
        parameters_combinations = list(product(*parameters_to_explore.values()))
        combinations_amount = len(parameters_combinations)
        if combinations_amount == 0:
            combinations_amount = 1
    
        for combination_index in range(combinations_amount):
            for instance_index in range(experiment.at('repeat')):
                full_output_path = os.path.join(self.output_path, f'parameters-combination-{combination_index}', f'simulation-{instance_index}')
                instance_name = f"{experiment.at('name')} ({self.timestamp}) - instance #{instance_index} of combination #{combination_index}"

                current_combination = parameters_combinations[combination_index]
                for parameter_index in range(len(current_combination)):
                    parameter_name = list(parameters_to_explore.keys())[parameter_index]
                    parameter_value = current_combination[parameter_index]
                    self.model_parameters().add(ModelParameter.suitable_by_value({'name':parameter_name, 'value':parameter_value}))

                execution = self._AWSAbstractSimulator__run_instance(instance_name, full_output_path, experiment)
        return execution