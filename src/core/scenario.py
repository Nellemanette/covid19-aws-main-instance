class Scenario:
    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('name')
        self._model_parameters = kwargs.get('model_parameters')
        self._environment = kwargs.get('environment')
        
    def environment(self, *args):
        try:
            environment_key = args[0]
        except:
            return self._environment
        else:
            return self._environment[environment_key]

    def model_parameters(self, *args):
        try:
            model_parameter_key = args[0]
        except:
            return self._model_parameters
        else:
            return self._model_parameters.at(model_parameter_key)