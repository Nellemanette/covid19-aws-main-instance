from datetime import datetime
from abc import ABC, abstractmethod
from src.headless.model_parameter import ModelParameter
from src.tools.suitable_class_finder import SuitableClassFinder
from src.experiments.experiment import Experiment, BuildingsViralLoad, GIS_Movements, \
    HospitalCapacity, MonitorsExperiment, ParameterExploration, ParameterOptimization

class AbstractSimulator(ABC):
    def __init__(self, scenario):
        self.scenario = scenario
        self._timestamp = None
    
    # ACCESSORS
    def scenario_name(self):
        return self.scenario.name
    
    def model_parameters(self, *args):
        return self.scenario.model_parameters(*args)
    
    def environment(self, *args):
        return self.scenario.environment(*args)

    def get_timestamp(self):
        if self._timestamp is None:
            self._timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        return self._timestamp
    
    @classmethod
    @abstractmethod
    def mode_name(cls):
        pass

    # SUITABLE SIMULATOR
    @classmethod
    def suitable_for(cls, execution):
        mode = execution.model_parameters().at('mode')
        simulator_subclass = SuitableClassFinder(cls).suitable_for(mode)
        return simulator_subclass(execution.scenario())

    @classmethod
    def can_handle(cls, a_mode):
        return cls.mode_name() == a_mode

    # SIMULATE 
    @abstractmethod
    def simulate(self, experiment):
        pass

    def __prepare_and_simulate(self, experiment_class, *args, **kwargs):
        parameters = {}
        for parameter_name, parameter_value in kwargs.items():
            parameters[parameter_name] = ModelParameter.suitable_by_value({'name':parameter_name, 'value':parameter_value})
        parameters['simulations_amount'] = ModelParameter.suitable_by_value({'name':'simulations_amount', 'value':1}) # <--- ONE SIMULATION BY DEFAULT, USE CLOUD MODE FOR MULTIPLE SIMULATIONS
        return self.simulate(experiment_class(*args, **parameters))

    # SIMULATE EXPERIMENTS
    def simulateBuildingsViralLoad(self, *args, **kwargs):
        return self.__prepare_and_simulate(BuildingsViralLoad, *args, **kwargs)
    
    def simulateGISMovements(self, *args, **kwargs):
        return self.__prepare_and_simulate(GIS_Movements, *args, **kwargs)
    
    def simulateHospitalCapacity(self, *args, **kwargs):
        return self.__prepare_and_simulate(HospitalCapacity, *args, **kwargs)
        
    def simulateMonitorsExperiment(self, *args, **kwargs):
        return self.__prepare_and_simulate(MonitorsExperiment, *args, **kwargs)
    
    def simulateParameterExploration(self, *args, repeat=3, real_symptomatic_amount=0, real_dead_amount=0, **kwargs):
        return self.__prepare_and_simulate(
            ParameterExploration, *args, repeat=repeat, 
            real_symptomatic_amount=real_symptomatic_amount, real_dead_amount=real_dead_amount, **kwargs)
        
    def simulateParameterOptimization(self, *args, repeat=3, iter_max=50, real_symptomatic_amount=0, real_dead_amount=0, **kwargs):
        return self.__prepare_and_simulate(
            ParameterOptimization, *args, repeat=repeat, iter_max=iter_max, 
            real_symptomatic_amount=real_symptomatic_amount, real_dead_amount=real_dead_amount, **kwargs)