# UNIT CONVERTIONS (TO UNITS USED BY GAMA PLATFORM)

identity = lambda current_value: current_value

km_per_hour_to_gama = lambda individual_speed: individual_speed / 3.6

minutes_to_seconds = lambda minutes: minutes * 60.0

days_to_gama = lambda days: days * 86400

km_to_meters = lambda kilometers: kilometers * 1000