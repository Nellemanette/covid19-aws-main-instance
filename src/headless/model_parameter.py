from abc import ABC, abstractmethod
from src.headless.conversions import identity
from src.tools.suitable_class_finder import SuitableClassFinder

# GAMA PARAMETER TYPES
_gama_string = 'STRING'
_gama_integer = 'INT'
_gama_float = 'FLOAT'
_gama_boolean = 'BOOLEAN'

# ABSTRACT MODEL PARAMETER
class ModelParameter(ABC):
    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('_name')
        self.type = kwargs.get('_type')
        self.value = kwargs.get('_value')
        self.conversion = kwargs.get('_conversion', identity)
    
    # ACCESSORS
    @classmethod
    @abstractmethod
    def encode(cls, value):
        pass

    @classmethod
    @abstractmethod
    def decode(cls, value):
        pass

    def encoded_value(self):
        return type(self).encode(self.conversion(self.value))

    # SUITABLE MODEL PARAMETER
    @classmethod
    def suitable_by_type(cls, parameter):
        parameter_subclass = SuitableClassFinder(cls).suitable_for(parameter['type'], suitable_method='can_handle_type', default_subclass=StringParameter)
        return parameter_subclass(_name=parameter['name'], _value=parameter_subclass.decode(parameter['value']))

    @classmethod
    def suitable_by_value(cls, parameter):
        parameter_subclass = SuitableClassFinder(cls).suitable_for(parameter['value'], suitable_method='can_handle_value', default_subclass=StringParameter)
        return parameter_subclass(_name=parameter['name'], _value=parameter['value'])

    @classmethod
    def can_handle_type(cls, gama_parameter_type):
        return cls.gama_parameter_type() == gama_parameter_type

    @classmethod
    def can_handle_value(cls, parameter_value):
        return type(parameter_value) == cls.related_class()

    @classmethod
    @abstractmethod
    def gama_parameter_type(cls):
        pass

# CONCRETE MODEL PARAMETERS
class StringParameter(ModelParameter):
    def __init__(self, *args, **kwargs):
        super(StringParameter, self).__init__(*args, _type=type(self).gama_parameter_type(), **kwargs)
    
    @classmethod
    def gama_parameter_type(cls):
        return _gama_string

    @classmethod
    def related_class(cls):
        return str

    @classmethod
    def encode(cls, value):
        return str(value)

    @classmethod
    def decode(cls, value):
        return str(value)
        
class IntegerParameter(ModelParameter):
    def __init__(self, *args, **kwargs):
        super(IntegerParameter, self).__init__(*args, _type=type(self).gama_parameter_type(), **kwargs)
    
    @classmethod
    def gama_parameter_type(cls):
        return _gama_integer

    @classmethod
    def related_class(cls):
        return int

    @classmethod
    def encode(cls, value):
        return str(int(value))

    @classmethod
    def decode(cls, value):
        return int(value)

class FloatParameter(ModelParameter):
    def __init__(self, *args, **kwargs):
        super(FloatParameter, self).__init__(*args, _type=type(self).gama_parameter_type(), **kwargs)
    
    @classmethod
    def gama_parameter_type(cls):
        return _gama_float

    @classmethod
    def related_class(cls):
        return float

    @classmethod
    def encode(cls, value):
        return str(float(value))
    
    @classmethod
    def decode(cls, value):
        return float(value)

class BooleanParameter(ModelParameter):
    def __init__(self, *args, **kwargs):
        super(BooleanParameter, self).__init__(*args, _type=type(self).gama_parameter_type(), **kwargs)
    
    @classmethod
    def gama_parameter_type(cls):
        return _gama_boolean

    @classmethod
    def related_class(cls):
        return bool

    @classmethod
    def encode(cls, value):
        return 'true' if value else 'false'

    @classmethod
    def decode(cls, value):
        return True if ('true' == value) else False