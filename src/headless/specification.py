import os
from lxml import etree as et
from src.headless.model_parameter import StringParameter

class HeadlessSpecification:
    def __init__(self, specification_file, simulation):
        self.specification_file = specification_file
        self.simulation = simulation
        self.current_simulation_id = 0

    def monitors(self):
        return [
            '#ElapsedTime', 'Datetime', '#Susceptible', '#Exposed', '#Asymptomatic', \
            '#Presymptomatic', '#Mild_Symptomatic', '#Severe_Symptomatic', '#ICU_Symptomatic', \
            '#Recovered', '#Dead', '#Symptomatic', '#Infected', '#Infectious']

    # AUXS
    def add_header_to(self, simulation_tag, experiment_name):
        simulation_tag.set('id', str(self.current_simulation_id))
        simulation_tag.set('sourcePath', self.simulation.environment().project_path('src','core','Main.gaml', ensure=False))
        simulation_tag.set('experiment', experiment_name)
        simulation_tag.set('finalStep', str(self.simulation.model_parameters().at('final_step')))
        
    def add_full_header_to(self, simulation_tag, experiment_name):
        self.add_header_to(simulation_tag, experiment_name)
        simulation_tag.set('until', '((individual count each.bio.is_infected()) = 0) and (individual_amount != (individual count each.bio.is_susceptible()))')

    def add(self, child_tag_name, model_parameter, parent_tag):
        child_tag = et.SubElement(parent_tag, child_tag_name)
        child_tag.set('name', model_parameter.name)
        child_tag.set('type', model_parameter.type)
        child_tag.set('value', model_parameter.encoded_value())

    def add_output_to(self, outputs_tag, output_id, output_name):
        output_tag = et.SubElement(outputs_tag, 'Output')
        output_tag.set('id', str(output_id))
        output_tag.set('name', output_name)
        output_tag.set('framerate', '1')

    # CORE
    def generate_for(self, experiment):
        experiment_plan = et.Element('Experiment_plan')

        for simulation_id in range(experiment.at('simulations_amount')):
            # CONFIGURE METADATA
            self.current_simulation_id = simulation_id
            simulation_tag = et.SubElement(experiment_plan, 'Simulation')
            experiment.configure_on(self, simulation_tag)
            
            # CONFIGURE MODEL PARAMETERS
            parameters_tag = et.SubElement(simulation_tag, 'Parameters')
            for parameter in self.simulation.model_parameters().values():
                self.add('Parameter', parameter, parameters_tag)

            # CONFIGURE GAMA OUTPUTS (LIKE MONITORS)
            outputs_tag = et.SubElement(simulation_tag, 'Outputs')
            index = 0
            for output in self.monitors():
                index = index+1
                self.add_output_to(outputs_tag, str(index),  output)
            
            # CONFIGURE SCENARIO DATA
            scenario_tag = et.SubElement(simulation_tag, 'Scenario')
            self.add('Data', StringParameter(_name='timestamp', _value=self.simulation.get_timestamp()), scenario_tag)
            self.add('Data', StringParameter(_name='scenario_name', _value=self.simulation.scenario_name()), scenario_tag)
            
            # CONFIGURE ENVIRONMENT DATA
            environment_tag = et.SubElement(simulation_tag, 'Environment')
            for parameter in self.simulation.environment().values():
                self.add('Data', parameter, environment_tag)

            # CONFIGURE EXPERIMENT DATA
            experiment_tag = et.SubElement(simulation_tag, 'Experiment')
            for parameter in experiment.values():
                self.add('Data', parameter, experiment_tag)

            # DUMP TO XML FILE
            file_contents = et.tostring(experiment_plan, encoding="UTF-8", method="xml", standalone=False)

            os.makedirs(os.path.dirname(self.specification_file), exist_ok=True)
            with open(self.specification_file, "wb") as experiment_plan_file:
                experiment_plan_file.write(file_contents)