import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

class PopulationSnapshotDumper:
    def __init__(self, execution, simulation_id=0):
        self.execution = execution
        self.simulation_id = simulation_id
        self.last_step = 0
        self.last_timestamp = ''
        self.snapshot_file_path = ''

    def start_dumping(self):
        # CALCULATE THE LAST STEP
        monitors_df = self.execution.load_csv_file_for("monitors", simulation_id=self.simulation_id)
        self.last_step = int(monitors_df[-1:]['Cycle'])
        self.last_timestamp = str(monitors_df[-1:]['Datetime'].values[0])

        # LOAD DATAFRAMES TO USE FROM CSV FILES
        population_df = self.execution.load_csv_file_for("population", simulation_id=self.simulation_id)
        cases_evolution_df = self.execution.load_csv_file_for("cases_evolution", simulation_id=self.simulation_id)
        states_evolution_df = self.execution.load_csv_file_for("states_evolution", simulation_id=self.simulation_id)

        # REMOVE AND RENAME COLUMNS
        cases_evolution_df.drop(columns=['location_case'], axis=1, inplace=True)
        cases_evolution_df.rename(columns={'current_date':'infection_date', 'current_cycle':'infection_cycle', 'new_case':'id'}, inplace=True)
        states_evolution_df.rename(columns={'current_date':'last_update_date', 'current_cycle':'last_update_cycle', 'person':'id', 'infections':'infections_amount'}, inplace=True)
        states_evolution_df['infections_amount'] = 0

        # KEEP THE LATEST STATE FOR EACH PERSON
        states_evolution_df = states_evolution_df.sort_values('last_update_cycle', ascending=False).drop_duplicates('id').sort_index()

        # MERGE LOADED DATAFRAMES
        snapshot_df = pd.merge(population_df, cases_evolution_df, how='left', on=['id', 'id'])
        snapshot_df = pd.merge(snapshot_df, states_evolution_df, how='left', on=['id', 'id'])

        # DETECT INFECTIONS
        infections_df = cases_evolution_df.loc[cases_evolution_df['disease_case'] == 'Local case']
        infections_df['infections_amount'] = infections_df.source_case.map(infections_df.source_case.value_counts())
        infections_df['infections'] = infections_df.source_case.map(lambda each : '|'.join(infections_df.loc[infections_df['source_case'] == each]['id']))
        infections_df = infections_df.drop_duplicates('source_case')
        infections_df.drop(columns=['infection_date', 'infection_cycle', 'id', 'disease_case'], axis=1, inplace=True)
        infections_df.rename(columns={'source_case':'id'}, inplace=True)

        # MERGE INFECTIONS DATA
        snapshot_df = pd.merge(snapshot_df, infections_df, how='left', on=['id', 'id'])
        snapshot_df['infections_amount'] = (np.max(snapshot_df[['infections_amount_x', 'infections_amount_y']], axis=1)).astype(int)
        snapshot_df.drop(columns=['infections_amount_x', 'infections_amount_y'], axis=1, inplace=True)

        # DUMP
        self.snapshot_file_path = self.execution.save_dataframe_as_csv_file(snapshot_df, 'population_snapshot', simulation_id=self.simulation_id)