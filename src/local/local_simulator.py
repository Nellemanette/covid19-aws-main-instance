import os
from subprocess import Popen, PIPE, check_output, run
from src.core.simulator import AbstractSimulator
from src.core.execution import Execution
from src.headless.specification import HeadlessSpecification

class LocalSimulator(AbstractSimulator):
    def __init__(self, *args, **kwargs):
        super(LocalSimulator, self).__init__(*args, **kwargs)

    @classmethod
    def mode_name(cls):
        return 'Local'

    # IN FUTURE; MAYBE SIMULATIONS_AMOUNT PARAMETER CAN BE USED IN XML HEADLESS FILE...
    def simulate(self, experiment):
        timestamp = self.get_timestamp()
        scenario_path = self.environment().resources_path_for(self.scenario_name())
        output_path = self.environment().output_path_for(self.scenario_name(), timestamp)
        headless_file = self.environment().output_path_for(self.scenario_name(), timestamp, 'headless.xml', ensure=False)
        
        self.model_parameters().\
            set_simulation_mode(type(self).mode_name()).\
            set_extra_parameters_file(self.environment().resources_path_for(self.scenario_name(), 'includes', 'extra_parameters.json')).\
            set_xml_headless_file_path(headless_file).\
            set_output_path(output_path).\
            set_resources_path(scenario_path)
        
        HeadlessSpecification(headless_file, self).generate_for(experiment)

        os.chdir(self.environment('java_jdk_path'))
        run([self.environment('gama_headless_sh'), headless_file, output_path])
        os.chdir(self.environment('project_path'))
        return Execution.defined_by(headless_file)