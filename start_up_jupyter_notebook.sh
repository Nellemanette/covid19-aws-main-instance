#!/usr/bin/env bash

local_flag=''
aws_flag=''
port='8888'

print_usage() {
  printf "Usage: -l (for local) | -a (for AWS) | -p (port, default 8888)"
}

while getopts 'lap:' flag; do
  case "${flag}" in
    l) local_flag='true' ;;
    a) aws_flag='true' ;;
    p) port="${OPTARG}" ;;
    *) print_usage
       exit 1 ;;
  esac
done

if [[ $aws_flag == 'true' ]]
then
  source ~/covid19-aws-main-instance/virtualenv-covid19/bin/activate &&
  export PATH=$PATH:~/.local/bin &&
  export PYTHONPATH=$PYTHONPATH:~/covid19-aws-main-instance/src &&
  screen -d -m -S JUPYTER jupyter notebook --no-browser --port="${port}"
else
  export PATH=$PATH:~/.local/bin &&
  export PYTHONPATH=$PYTHONPATH:~/covid19-aws-main-instance/src &&
  screen -d -m -S JUPYTER jupyter notebook --no-browser --port="${port}"
fi


