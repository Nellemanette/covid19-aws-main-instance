import unittest
from src.headless.conversions import *

class TestConversions(unittest.TestCase):
    def test_identity(self):
        self.assertEqual(identity('Hello'), 'Hello')
        self.assertEqual(identity(123), 123)
        self.assertEqual(identity(3.1415), 3.1415)
        self.assertEqual(identity(True), True)
        self.assertEqual(identity(False), False)

    def test_km_per_hour_to_gama(self):
        self.assertEqual(km_per_hour_to_gama(1), 0.2777777777777778)
        self.assertEqual(km_per_hour_to_gama(3.6), 1.0)

    def test_minutes_to_seconds(self):
        self.assertEqual(minutes_to_seconds(1/60), 1)
        self.assertEqual(minutes_to_seconds(1), 60)
        self.assertEqual(minutes_to_seconds(60), 3600)

    def test_days_to_gama(self):
        self.assertEqual(days_to_gama(1), 86400)
        self.assertEqual(days_to_gama(7), 604800)
        self.assertEqual(days_to_gama(30), 2592000)

    def test_km_to_meters(self):
        self.assertEqual(km_to_meters(1), 1000)
        self.assertEqual(km_to_meters(0.001), 1)
        self.assertEqual(km_to_meters(10), 10000)

if __name__ == '__main__':
    unittest.main()