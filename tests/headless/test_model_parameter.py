import unittest
from src.headless.model_parameter import ModelParameter, _gama_boolean, _gama_integer, _gama_float, _gama_string

class TestModelParameter(unittest.TestCase):
    def test_gama_boolean_type(self):
        self.assertEqual(_gama_boolean, 'BOOLEAN', 'Python bool is BOOLEAN in GAMA Platform.')

    def test_gama_float_type(self):
        self.assertEqual(_gama_float, 'FLOAT', 'Python float is FLOAT in GAMA Platform.')

    def test_gama_integer_type(self):
        self.assertEqual(_gama_integer, 'INT', 'Python int is INT in GAMA Platform.')

    def test_gama_string_type(self):
        self.assertEqual(_gama_string, 'STRING', 'Python str is STRING in GAMA Platform.')        

    def test_suitable_by_boolean_true_type(self):
        parameter_dict = {'name':'my_parameter', 'type':'BOOLEAN', 'value':'true'}
        model_parameter = ModelParameter.suitable_by_type(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'BOOLEAN')
        self.assertEqual(model_parameter.related_class(), bool)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'BOOLEAN')
        self.assertEqual(model_parameter.value, True)
        self.assertEqual(model_parameter.encoded_value(), 'true')

    def test_suitable_by_boolean_false_type(self):
        parameter_dict = {'name':'my_parameter', 'type':'BOOLEAN', 'value':'false'}
        model_parameter = ModelParameter.suitable_by_type(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'BOOLEAN')
        self.assertEqual(model_parameter.related_class(), bool)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'BOOLEAN')
        self.assertEqual(model_parameter.value, False)
        self.assertEqual(model_parameter.encoded_value(), 'false')

    def test_suitable_by_boolean_true_value(self):
        parameter_dict = {'name':'my_parameter', 'value':True}
        model_parameter = ModelParameter.suitable_by_value(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'BOOLEAN')
        self.assertEqual(model_parameter.related_class(), bool)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'BOOLEAN')
        self.assertEqual(model_parameter.value, True)
        self.assertEqual(model_parameter.encoded_value(), 'true')

    def test_suitable_by_boolean_value(self):
        parameter_dict = {'name':'my_parameter', 'value':False}
        model_parameter = ModelParameter.suitable_by_value(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'BOOLEAN')
        self.assertEqual(model_parameter.related_class(), bool)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'BOOLEAN')
        self.assertEqual(model_parameter.value, False)
        self.assertEqual(model_parameter.encoded_value(), 'false')

    def test_suitable_by_float_type(self):
        parameter_dict = {'name':'my_parameter', 'type':'FLOAT', 'value':'3.1415'}
        model_parameter = ModelParameter.suitable_by_type(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'FLOAT')
        self.assertEqual(model_parameter.related_class(), float)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'FLOAT')
        self.assertEqual(model_parameter.value, 3.1415)
        self.assertEqual(model_parameter.encoded_value(), '3.1415')

    def test_suitable_by_float_value(self):
        parameter_dict = {'name':'my_parameter', 'value':3.1415}
        model_parameter = ModelParameter.suitable_by_value(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'FLOAT')
        self.assertEqual(model_parameter.related_class(), float)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'FLOAT')
        self.assertEqual(model_parameter.value, 3.1415)
        self.assertEqual(model_parameter.encoded_value(), '3.1415')

    def test_suitable_by_integer_type(self):
        parameter_dict = {'name':'my_parameter', 'type':'INT', 'value':'123'}
        model_parameter = ModelParameter.suitable_by_type(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'INT')
        self.assertEqual(model_parameter.related_class(), int)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'INT')
        self.assertEqual(model_parameter.value, 123)
        self.assertEqual(model_parameter.encoded_value(), '123')

    def test_suitable_by_integer_value(self):
        parameter_dict = {'name':'my_parameter', 'value':123}
        model_parameter = ModelParameter.suitable_by_value(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'INT')
        self.assertEqual(model_parameter.related_class(), int)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'INT')
        self.assertEqual(model_parameter.value, 123)
        self.assertEqual(model_parameter.encoded_value(), '123')

    def test_suitable_by_string_type(self):
        parameter_dict = {'name':'my_parameter', 'type':'STRING', 'value':'My Value'}
        model_parameter = ModelParameter.suitable_by_type(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'STRING')
        self.assertEqual(model_parameter.related_class(), str)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'STRING')
        self.assertEqual(model_parameter.value, 'My Value')
        self.assertEqual(model_parameter.encoded_value(), 'My Value')

    def test_suitable_by_string_value(self):
        parameter_dict = {'name':'my_parameter', 'value':'My Value'}
        model_parameter = ModelParameter.suitable_by_value(parameter_dict)
        self.assertEqual(model_parameter.gama_parameter_type(), 'STRING')
        self.assertEqual(model_parameter.related_class(), str)
        self.assertEqual(model_parameter.name, 'my_parameter')
        self.assertEqual(model_parameter.type, 'STRING')
        self.assertEqual(model_parameter.value, 'My Value')
        self.assertEqual(model_parameter.encoded_value(), 'My Value')

if __name__ == '__main__':
    unittest.main()